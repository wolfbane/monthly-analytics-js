/*jshint multistr: true */
/**
 * Bazaarvoice Monthly Analytics
 * Author: Mashbox
 *
 * Description:
 * String output for the Monthly Analytics email
 */

 var Moment = require('moment');

(function () {
  function MonthlyAnalytics(data) {
    this.data = data;
    return this;
  }

  MonthlyAnalytics.prototype.getConversionLift = function () {
    var cvr_goal_name = this.data.cvr_goal_name;

    if (cvr_goal_name) {
      return '<p style="margin: 0;margin-bottom: 30px;font-family:verdana,sans-serif;font-size: 12px;font-weight: 400;line-height: 1.6;color: #003c4e;"><strong style="font-weight: 600;">' + cvr_goal_name + ' lift</strong><br/> \
              Percent change (lift) in between visitors who used Bazaarvoice and who did not use Bazaarvoice, calculated for e-commerce conversion rate or a non-commerce goal (if no e-commerce). Ratings &amp; Reviews only. 3rd-party measurement.<br/> \
              Reports &gt; Dashboards &gt; Conversion Impact Report</p>';
    }
  };

  MonthlyAnalytics.prototype.getCurrentMonth = function () {
    return Moment(this.data.report_date).format('MMMM YYYY');
  };

  MonthlyAnalytics.prototype.getCustomerSite = function () {
    return this.data.client;
  };

  MonthlyAnalytics.prototype.getPageviewsTotal = function () {
    var total_pvs = this.nullToVal(this.data.total_pvs);
    var total_pvs_delta = this.nullToVal(this.data.total_pvs_delta);
    var polarity = this.polarityVal(total_pvs_delta);

    return '<table cellpadding="0" cellspacing="0" border="0"> \
              <tr> \
                <td style="margin: 0;font-family:verdana,sans-serif;font-size: 22px;font-weight: 600;line-height: 1.6;color: #ffffff;overflow: hidden;" class="big-number">\
                  ' + this.formatNumber(total_pvs) + ' \
                </td> \
                <td align="center" style="margin: 0;padding-left: 5px;padding-right: 5px;font-family:verdana,sans-serif;font-size: 12px;font-weight: 400;line-height: 1.6;color: ' + polarity.color + ';overflow: hidden;" class="small-number">\
                  ' + this.formatNumber(total_pvs_delta) + '% \
                </td> \
                <td style="width: 13px;"> \
                  ' + polarity.icon + ' \
                </td> \
              </tr> \
            </table>';
  };

  MonthlyAnalytics.prototype.getPageviewsReviews = function () {
    var pv_w_rev = this.nullToVal(this.data.pv_w_rev);
    var pv_w_rev_delta = this.nullToVal(this.data.pv_w_rev);
    var polarity = this.polarityVal(pv_w_rev_delta);

    return '<table cellpadding="0" cellspacing="0" border="0"> \
              <tr> \
                <td style="margin: 0;font-family:verdana,sans-serif;font-size: 22px;font-weight: 600;line-height: 1.6;color: #ffffff;overflow: hidden;" class="big-number">\
                  ' + this.formatNumber(pv_w_rev) + ' \
                </td> \
                <td align="center" style="margin: 0;padding-left: 5px;padding-right: 5px;font-family:verdana,sans-serif;font-size: 12px;font-weight: 400;line-height: 1.6;color: ' + polarity.color + ';overflow: hidden;" class="small-number">\
                  ' + this.formatNumber(pv_w_rev_delta) + '% \
                </td> \
                <td style="width: 13px;"> \
                  ' + polarity.icon + ' \
                </td> \
              </tr> \
            </table>';
  };

  MonthlyAnalytics.prototype.getPageviewsROIBeacon = function () {

    var cvr_goal_name = this.data.cvr_goal_name;

    if (cvr_goal_name) {
      var cvr_lift = this.nullToVal(this.data.cvr_lift);
      var cvr_delta = this.nullToVal(this.data.cvr_delta);
      var polarity = this.polarityVal(cvr_delta);

      return '<table cellpadding="0" cellspacing="0" border="0"> \
                <tr> \
                  <td style="margin: 0;font-family:verdana,sans-serif;font-size: 22px;font-weight: 600;line-height: 1.6;color: #ffffff;overflow: hidden;" class="big-number">\
                    ' + this.formatNumber(cvr_lift) + ' \
                  </td> \
                  <td align="center" style="margin: 0;padding-left: 5px;padding-right: 5px;font-family:verdana,sans-serif;font-size: 12px;font-weight: 400;line-height: 1.6;color: ' + polarity.color + ';overflow: hidden;" class="small-number">\
                    ' + this.formatNumber(cvr_delta) + '% \
                  </td> \
                  <td style="width: 13px;"> \
                    ' + polarity.icon + ' \
                  </td> \
                </tr> \
              </table>';

    // No Goal Name
    } else {
      return '<p style="margin: 0;font-family:verdana,sans-serif;font-size: 14px;font-weight: 400;line-height: 1.4;color: #00ab96;">Get the ROI Beacon<br/>to view your<br/>conversion lift.</p> \
              <p style="margin: 0;margin-top: 12px;"><a href="http://knowledge.bazaarvoice.com/roi-beacon/" target="_blank"><img src="http://resources.bazaarvoice.com/rs/848-PGD-174/images/monthly_report_button_roibeacon.png" width="130" height="32" alt="ROI Beacon" class="image_fix" style="border:none;display: block;"/></a></p>';
    }
  };

  MonthlyAnalytics.prototype.getDataCollection = function () {
    var report_date = Moment(this.data.report_date);

    var month_tm = report_date.format('MMM');
    var month_lm = report_date.clone().subtract(1, 'month').format('MMM');

    var new_rev_lm = this.nullToVal(this.data.new_rev_lm);
    var new_rev_tm = this.nullToVal(this.data.new_rev_tm);
    var new_rev_delta = this.nullToVal(this.data.new_rev_delta);

    var avg_daily_lm = this.nullToVal(this.data.avg_daily_lm);
    var avg_daily_tm = this.nullToVal(this.data.avg_daily_tm);
    var avg_daily_delta = this.nullToVal(this.data.avg_daily_delta);

    var rev_app_lm = this.nullToVal(this.data.rev_app_lm);
    var rev_app_tm = this.nullToVal(this.data.rev_app_tm);
    var rev_app_delta = this.nullToVal(this.data.rev_app_delta);

    var avg_rating_lm = this.nullToVal(this.data.avg_rating_lm);
    var avg_rating_tm = this.nullToVal(this.data.avg_rating_tm);
    var avg_rating_delta = this.nullToVal(this.data.avg_rating_delta);

    var rec_lm = this.nullToVal(this.data.rec_lm);
    var rec_tm = this.nullToVal(this.data.rec_tm);
    var rec_delta = this.nullToVal(this.data.rec_delta);

    return '<table width="100%" cellpadding="0" cellspacing="0" border="0" class="table-data"> \
              <tr> \
                <th style="height:36px;border-bottom: 1px solid #d8d8d8;">&nbsp;</th> \
                <th data-width="62" style="width: 62px;border-bottom: 1px solid #d8d8d8;" bgcolor="#e8e8e5"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + month_lm + '</span></th> \
                <th data-width="62" style="width: 62px;border-bottom: 1px solid #d8d8d8;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + month_tm + '</span></th> \
                <th data-width="62" style="width: 62px;border-bottom: 1px solid #d8d8d8;" bgcolor="#e8e8e5"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">% chg</span></th> \
              </tr> \
    					<tr> \
                <td style="height:32px;border-bottom: 1px solid #d8d8d8;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 13px;font-weight: 400;line-height: 1.6;color: #003c4e;">New reviews</span></td> \
                <td align="center" style="border-bottom: 1px solid #d8d8d8;" bgcolor="#e8e8e5"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + this.prettyNumber(new_rev_lm) + '</span></td> \
                <td align="center" style="border-bottom: 1px solid #d8d8d8;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + this.prettyNumber(new_rev_tm) + '</span></td> \
                <td align="center" style="border-bottom: 1px solid #d8d8d8;" bgcolor="#e8e8e5"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 400;line-height: 1.6;color: #003c4e;">' + (new_rev_delta > 0 ? '+' : '') + this.prettyNumber(new_rev_delta) + '%</span></td> \
              </tr> \
              <tr> \
                <td style="height:32px;border-bottom: 1px solid #d8d8d8;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 13px;font-weight: 400;line-height: 1.6;color: #003c4e;">Average daily reviews</span></td> \
                <td align="center" style="border-bottom: 1px solid #d8d8d8;" bgcolor="#e8e8e5"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + this.prettyNumber(avg_daily_lm.toFixed(1)) + '</span></td> \
                <td align="center" style="border-bottom: 1px solid #d8d8d8;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + this.prettyNumber(avg_daily_tm.toFixed(1)) + '</span></td> \
                <td align="center" style="border-bottom: 1px solid #d8d8d8;" bgcolor="#e8e8e5"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 400;line-height: 1.6;color: #003c4e;">' + (avg_daily_delta > 0 ? '+' : '') + this.prettyNumber(avg_daily_delta) + '%</span></td> \
              </tr> \
              <tr> \
                <td style="height:32px;border-bottom: 1px solid #d8d8d8;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 13px;font-weight: 400;line-height: 1.6;color: #003c4e;">Reviews approved</span></td> \
                <td align="center" style="border-bottom: 1px solid #d8d8d8;" bgcolor="#e8e8e5"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + this.prettyNumber(rev_app_lm) + '%</span></td> \
                <td align="center" style="border-bottom: 1px solid #d8d8d8;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + this.prettyNumber(rev_app_tm) + '%</span></td> \
                <td align="center" style="border-bottom: 1px solid #d8d8d8;" bgcolor="#e8e8e5"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 400;line-height: 1.6;color: #003c4e;">' + (rev_app_delta > 0 ? '+' : '') + this.prettyNumber(rev_app_delta) + '%</span></td> \
              </tr> \
              <tr> \
                <td style="height:32px;border-bottom: 1px solid #d8d8d8;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 13px;font-weight: 400;line-height: 1.6;color: #003c4e;">Average rating</span></td> \
                <td align="center" style="border-bottom: 1px solid #d8d8d8;" bgcolor="#e8e8e5"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + this.prettyNumber(avg_rating_lm) + '</span></td> \
                <td align="center" style="border-bottom: 1px solid #d8d8d8;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + this.prettyNumber(avg_rating_tm) + '</span></td> \
                <td align="center" style="border-bottom: 1px solid #d8d8d8;" bgcolor="#e8e8e5"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 400;line-height: 1.6;color: #003c4e;">' + (avg_rating_delta > 0 ? '+' : '') + this.prettyNumber(avg_rating_delta) + '%</span></td> \
              </tr> \
              <tr> \
                <td style="height: 32px;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 13px;font-weight: 400;line-height: 1.6;color: #003c4e;">Recommend</span></td> \
                <td align="center" bgcolor="#e8e8e5"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + this.prettyNumber(rec_lm) + '%</span></td> \
                <td align="center"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + this.prettyNumber(rec_tm) + '%</span></td> \
                <td align="center" bgcolor="#e8e8e5"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 400;line-height: 1.6;color: #003c4e;">' + this.prettyNumber(rec_delta) + '%</span></td> \
              </tr> \
              <tr> \
                <td style="height: 4px;"><img src="http://resources.bazaarvoice.com/rs/848-PGD-174/images/monthly_report_transparent.png" width="100%" style="height: 4px;max-height:4px;display:block;" alt="" class="image_fix"/></td> \
                <td style="height: 4px;" bgcolor="#e8e8e5"><img src="http://resources.bazaarvoice.com/rs/848-PGD-174/images/monthly_report_transparent.png" width="100%" style="height: 4px;max-height:4px;display:block;" alt="" class="image_fix"/></td> \
                <td style="height: 4px;"><img src="http://resources.bazaarvoice.com/rs/848-PGD-174/images/monthly_report_transparent.png" width="100%" style="height: 4px;max-height:4px;display:block;" alt="" class="image_fix"/></td> \
                <td style="height: 4px;" bgcolor="#e8e8e5"><img src="http://resources.bazaarvoice.com/rs/848-PGD-174/images/monthly_report_transparent.png" width="100%" style="height: 4px;max-height:4px;display:block;" alt="" class="image_fix"/></td> \
              </tr> \
            </table>';
  };

  MonthlyAnalytics.prototype.getDataTopRatedProducts = function () {
    var t_p1_name = this.data.t_p1_name;
    var t_p1_rating = this.nullToVal(this.data.t_p1_rating);
    var t_p1_count = this.nullToVal(this.data.t_p1_count);
    var t_p2_name = this.data.t_p1_name;
    var t_p2_rating = this.nullToVal(this.data.t_p2_rating);
    var t_p2_count = this.nullToVal(this.data.t_p2_count);
    var t_p3_name = this.data.t_p1_name;
    var t_p3_rating = this.nullToVal(this.data.t_p3_rating);
    var t_p3_count = this.nullToVal(this.data.t_p3_count);
    var op = '';

    if (t_p1_name || t_p2_name || t_p3_name) {
      op += '<table width="100%" cellpadding="0" cellspacing="0" border="0" class="table-data"> \
              <tr> \
                <th style="height:36px;border-bottom: 1px solid #aedfd4;" align="left"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">Product</span></th> \
                <th data-width="65" style="width:65px;border-bottom: 1px solid #aedfd4;" bgcolor="#d0e9e1"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">Rating</span></th> \
                <th data-width="65" style="width:65px;border-bottom: 1px solid #aedfd4;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">Count</span></th> \
              </tr>';

      if (t_p1_name) {
        op += '<tr> \
                <td style="height:32px;border-bottom: 1px solid #aedfd4;padding-top:5px;padding-bottom:5px;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 13px;font-weight: 400;line-height: 1.6;color: #003c4e;">'+ t_p1_name + '</span></td> \
                <td align="center" style="border-bottom: 1px solid #aedfd4;" bgcolor="#d0e9e1"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + t_p1_rating.toFixed(2) + '</span></td> \
                <td align="center" style="border-bottom: 1px solid #aedfd4;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + t_p1_count + '</span></td> \
              </tr>';
      }

      if (t_p2_name) {
        op += '<tr> \
                <td style="height:32px;border-bottom: 1px solid #aedfd4;padding-top:5px;padding-bottom:5px;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 13px;font-weight: 400;line-height: 1.6;color: #003c4e;">' + t_p2_name + '</span></td> \
                <td align="center" style="border-bottom: 1px solid #aedfd4;" bgcolor="#d0e9e1"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + t_p2_rating.toFixed(2) + '</span></td> \
                <td align="center" style="border-bottom: 1px solid #aedfd4;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + t_p2_count + '</span></td> \
              </tr>';
      }

      if (t_p3_name) {
        op += '<tr> \
                <td style="height:32px;padding-top:5px;padding-bottom:5px;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 13px;font-weight: 400;line-height: 1.6;color: #003c4e;">' + t_p3_name + '</span></td> \
                <td align="center" bgcolor="#d0e9e1"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + t_p3_rating.toFixed(2) + '</span></td> \
                <td align="center"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + t_p3_count + '</span></td> \
              </tr>';
      }

      op += '<tr> \
              <td style="height: 4px;"><img src="http://resources.bazaarvoice.com/rs/848-PGD-174/images/monthly_report_transparent.png" width="100%" style="height: 4px;max-height:4px;display: block;" alt="" class="image_fix"/></td> \
              <td style="height: 4px;" bgcolor="#d0e9e1"><img src="http://resources.bazaarvoice.com/rs/848-PGD-174/images/monthly_report_transparent.png" width="100%" style="height: 4px;max-height:4px;display: block;" alt="" class="image_fix"/></td> \
              <td style="height: 4px;"><img src="http://resources.bazaarvoice.com/rs/848-PGD-174/images/monthly_report_transparent.png" width="100%" style="height: 4px;max-height:4px;display: block;" alt="" class="image_fix"/></td> \
            </tr> \
          </table>';
    }

    return op;
  };

  MonthlyAnalytics.prototype.getDataLowestRatedProducts = function () {
    var l_p1_name = this.data.l_p1_name;
    var l_p1_rating = this.nullToVal(this.data.l_p1_rating);
    var l_p1_count = this.nullToVal(this.data.l_p1_count);
    var l_p2_name = this.data.l_p1_name;
    var l_p2_rating = this.nullToVal(this.data.l_p2_rating);
    var l_p2_count = this.nullToVal(this.data.l_p2_count);
    var l_p3_name = this.data.l_p1_name;
    var l_p3_rating = this.nullToVal(this.data.l_p3_rating);
    var l_p3_count = this.nullToVal(this.data.l_p3_count);
    var op = '';

    if (l_p1_name || l_p2_name || l_p3_name) {
      op += '<table width="100%" cellpadding="0" cellspacing="0" border="0" class="table-data"> \
              <tr> \
                <th style="height: 36px;border-bottom: 1px solid #d8d8d8;" align="left"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">Product</span></th> \
                <th data-width="65" style="width: 65px;border-bottom: 1px solid #d8d8d8;" bgcolor="#e8e8e5"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">Rating</span></th> \
                <th data-width="65" style="width: 65px;border-bottom: 1px solid #d8d8d8;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">Count</span></th> \
              </tr>';

      if (l_p1_name) {
        op += '<tr> \
                <td style="height: 32px;border-bottom: 1px solid #d8d8d8;padding-top:5px;padding-bottom:5px;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 13px;font-weight: 400;line-height: 1.6;color: #003c4e;">' + l_p1_name + '</span></td> \
                <td align="center" style="border-bottom: 1px solid #d8d8d8;" bgcolor="#e8e8e5"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + l_p1_rating.toFixed(2) + '</span></td> \
                <td align="center" style="border-bottom: 1px solid #d8d8d8;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + l_p1_count + '</span></td> \
              </tr>';
      }

      if (l_p2_name) {
        op += '<tr> \
                <td style="height: 32px;border-bottom: 1px solid #d8d8d8;padding-top:5px;padding-bottom:5px;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 13px;font-weight: 400;line-height: 1.6;color: #003c4e;">' + l_p2_name + '</span></td> \
                <td align="center" style="border-bottom: 1px solid #d8d8d8;" bgcolor="#e8e8e5"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + l_p2_rating.toFixed(2) + '</span></td> \
                <td align="center" style="border-bottom: 1px solid #d8d8d8;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + l_p2_count + '</span></td> \
              </tr>';
      }

      if (l_p3_name) {
        op += '<tr> \
                <td style="height:32px;padding-top:5px;padding-bottom:5px;"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 13px;font-weight: 400;line-height: 1.6;color: #003c4e;">' + l_p3_name + '</span></td> \
                <td align="center" bgcolor="#e8e8e5"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + l_p3_rating.toFixed(2) + '</span></td> \
                <td align="center"><span style="margin: 0;font-family:verdana,sans-serif;font-size: 12px;font-weight: 600;line-height: 1.6;color: #003c4e;">' + l_p3_count + '</span></td> \
              </tr>';
      }

      op += '<tr> \
              <td style="height: 4px;"><img src="http://resources.bazaarvoice.com/rs/848-PGD-174/images/monthly_report_transparent.png" width="100%" style="height: 4px;max-height:4px;display: block;" alt="" class="image_fix"/></td> \
              <td style="height: 4px;" bgcolor="#e8e8e5"><img src="http://resources.bazaarvoice.com/rs/848-PGD-174/images/monthly_report_transparent.png" width="100%" style="height: 4px;max-height:4px;display: block;" alt="" class="image_fix"/></td> \
              <td style="height: 4px;"><img src="http://resources.bazaarvoice.com/rs/848-PGD-174/images/monthly_report_transparent.png" width="100%" style="height: 4px;max-height:4px;display: block;" alt="" class="image_fix"/></td> \
            </tr> \
          </table>';
    }

    return op;
  };

  MonthlyAnalytics.prototype.nullToVal = function (v, val) {
    val = val || 0;

    if (!v) { v = val; }

    return v;
  };

  MonthlyAnalytics.prototype.formatNumber = function (num) {
    var format = parseInt(num, 10);
    var raw;

    // Millions!
    if (num > 999999) {
    	raw = (num / 1000000);
    	format = raw.toFixed(1) + 'm';
    }
    // Thousands!
    if (num > 999) {
    	raw = (num / 1000);
    	format = raw.toFixed(1) + 'k';
    }

    return format;
  };

  MonthlyAnalytics.prototype.prettyNumber = function (num, delimiter) {
    var regex = /(\d+)(\d{3})/;
    num = (typeof num === 'number') ? num.toString() : num;
    delimiter = delimiter || ',';

    while (regex.test(num)) {
      num = num.replace(regex, "$1" + delimiter + "$2");
    }

    return num;
  };


  MonthlyAnalytics.prototype.polarityVal = function (num) {
    num = parseInt(num, 10);
    var icon = '<img src="http://resources.bazaarvoice.com/rs/848-PGD-174/images/monthly_report_icon_neutral.png" alt="" class="image_fix" style="width:13px;height:13px;display:block;"/>';
    var color = "#e2c759";

    if (num < 0) {
      icon = '<img src="http://resources.bazaarvoice.com/rs/848-PGD-174/images/monthly_report_icon_negative.png" width="13" height="13" alt="" class="image_fix" style="display:block;"/>';
    	color = "#e86d1f";
    }

    if (num > 0) {
      icon = '<img src="http://resources.bazaarvoice.com/rs/848-PGD-174/images/monthly_report_icon_positive.png" width="13" height="13" alt="" class="image_fix" style="display:block;"/>';
    	color = "#c1cb55";
    }

    return {
      icon: icon,
      color: color
    };
  };

  window.MonthlyAnalytics = MonthlyAnalytics;
})();
