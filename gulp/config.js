var dst = './build';
var src = './src';
var tmp = './tmp';

// TODO: Steve -- change this. Instead of having one config file for all tasks. Move the config for each task to its task file.

module.exports = {
  // Environment Paths
  env: {
    dst: dst,
    src: src,
    tmp: tmp
  },

  // Browser Sync
  browserSync: {
    server: {
      // We're serving the src folder as well
      // for sass sourcemap linking
      baseDir: [tmp]
    },
    files: [
      tmp + '/**',
      // Exclude Map files
      '!' + tmp + '/**.map'
    ]
  },

  // Browserify
  browserify: {
    // Enable source maps
    debug: false,
    // Additional file extentions to make optional
    extensions: ['.coffee', '.hbs'],
    // Add in some aliases
    aliases: {
    },
    // A separate bundle will be generated for each
    // bundle config in the list below
    bundleConfigs: [{
      entries: src + '/monthly-analytics.js',
      dest: tmp,
      outputName: 'monthly-analytics.js'
    }, {
      entries: './test/index.js',
      dest: tmp,
      outputName: 'client-test.js',
      external: []
    }]
  },

  // JS
  js: {
    path: '/',
    files: '/**/*.js'
  },

  // Markup
  markup: {
    path: '',
    files: '/*.{html,txt,ico}'
  },

  // Example files
  example: {
    path: 'example',
    files: '/**/*'
  }
};
