# monthly-analytics.js

Output formatted text blocks from the customer data CSV

# Usage

First, create a MonthlyAnalytics Object by passing the raw customer data. You can then requests the formatted content from the object.

```js
var ma = new MonthlyAnalytics({
  client: 'starwood',
  report_date: '2015-12-01',
  total_pvs: 462124,
  total_pvs_delta: -55,
  pv_w_rev: 98,
  pv_w_rev_delta: -1,
  cvr_goal_name: 'ReserveNow',
  cvr_lift: 900,
  cvr_delta: -68,
  new_rev_lm: 1981,
  new_rev_tm: 2180,
  new_rev_delta: 10,
  avg_daily_lm: 66,
  avg_daily_tm: 70.3,
  avg_daily_delta: 7,
  rev_app_lm: 85,
  rev_app_tm: 84,
  rev_app_delta: -1,
  avg_rating_lm: 4,
  avg_rating_tm: 4,
  avg_rating_delta: '',
  rec_lm: 77,
  rec_tm: 76,
  rec_delta: -1,
  t_p1_name: 'Sheraton Qingyuan Lion Lake Resort',
  t_p1_rating: 5,
  t_p1_count: 30,
  t_p2_name: 'The Westin Lagunamar Ocean Resort Villas & Spa, Cancun',
  t_p2_rating: 4.87,
  t_p2_count: 8,
  t_p3_name: 'Sheraton New Orleans Hotel',
  t_p3_rating: 5,
  t_p3_count: 5,
  l_p1_name: 'Sheraton Phoenix Airport Hotel Tempe',
  l_p1_rating: 1,
  l_p1_count: 2,
  l_p2_name: "Vistana's Beach Club",
  l_p2_rating: 1,
  l_p2_count: 2,
  l_p3_name: 'The Great Wall Sheraton Hotel Beijing',
  l_p3_rating: 1.5,
  l_p3_count: 2
});

document.write(ma.getCustomerSite());

```

## Content Functions

You can request each of these functions.

### getConversionLift

### getCurrentMonth

### getCustomerSite

### getPageviewsTotal

### getPageviewsReviews

### getPageviewsROIBeacon

### getDataCollection

### getDataTopRatedProducts

### getDataLowestRatedProducts

# License

MIT-License (see LICENSE file).
